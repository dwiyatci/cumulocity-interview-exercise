/**
 * Created by glenn on 23/03/16.
 */

import { AlarmsController } from './alarms.controller';

function alarmsDirective() {
  return {
    restrict    : 'E',
    template    : require('./alarms.directive.html'),
    controller  : AlarmsController,
    controllerAs: 'vm'
    //replace : true
  };
}

export { alarmsDirective };
