/**
 * Created by glenn on 23/03/16.
 */

alarmsDataService.$inject = ['$http'];

function alarmsDataService($http) {
  return $http({
    method: 'GET',
    url   : 'https://demos.cumulocity.com/alarm/alarms',
    params: {
      pageSize: 2000,
      resolved: false
    }
  }).then(response => response.data.alarms);
}

export { alarmsDataService };
