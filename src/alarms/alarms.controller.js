/**
 * Created by glenn on 23/03/16.
 */

AlarmsController.$inject = ['alarmsDataService'];

function AlarmsController(alarmsDataService) {
  const vm = this;

  vm.alarms = [];

  activate();

  function activate() {
    return alarmsDataService.then(alarms => {
      vm.alarms = alarms;
    });
  }
}

export { AlarmsController };

// life, universe, and everything
// 6 * 7 = 42
