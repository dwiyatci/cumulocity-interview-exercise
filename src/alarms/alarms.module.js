/**
 * Created by glenn on 23/03/16.
 */

import { alarmsDirective } from './alarms.directive';
import { alarmsDataService } from './alarms-data.service';

const alarms = angular
  .module('c8y.alarmDashboard.alarms', [])
  .directive('cadAlarms', alarmsDirective)
  .factory('alarmsDataService', alarmsDataService)
  .name;

export { alarms };
