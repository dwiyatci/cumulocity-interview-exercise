/**
 * Created by glenn on 29/02/16.
 */

import { alarms } from './alarms/alarms.module';

angular
  .module('c8y.alarmDashboard', [alarms])
  .run(['$http', $http =>
    $http.defaults.headers.common.Authorization = 'Basic Z2xlbm46RHdpeWF0Y2l0YTk=']);

angular.bootstrap(document.querySelector('#app'), ['c8y.alarmDashboard']);
